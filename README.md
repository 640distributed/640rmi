# REMOTE METHOD INVOCATION #

## Introduction ##

This is a platform for invoking methods from JAVA objects located on other machines, like the JAVA RMI
facility. Additionally, it allows registry to serve for multiple clients and servers, and remote object may export to a specified port.Callback feature also implemented.

Note: the remote interface in both peers must have the same package name && class name, the _stub and _imp class must be in the same package as the remote interface in both peers.

NEW!! StubCompiler added! Read How to run for more info.

## how it works ##
![design.jpg](https://bitbucket.org/repo/KGAgqz/images/1898685777-design.jpg)

1.server registers a dispatcher on a specific port to communicator,communicator starts a listening server on that port.

2.remote object exports to that dispatcher on <uuid,localRef>, getting an stub with ROR

3.remote object registers to registry server on <name,ROR>.

4.client sends request looking for a remote object by a name to registry server

5.registry server responds with a ROR of that object

6.client creates a stub for that object with the ROR

7.client calls a method on that stub(it treats the stub as the real remote object)

8.stub [1]checks parameter objects, marshalls parameter objs, method name,ROR,hasReturn into a Message,passes message to communicator.

9.communicator passes the message to a host and port according to ROR of message.

10.server side communicator receives that message, passes to dispatcher registered on that port.

11~13.dispatcher unmarshalls message, get localRef using the uuid of the ROR in message, invokes that method of localRef (using Reflection),[2]checks the return object and return it to communicator

14~16.communicator returns a return object(till then the client’s communicator blocks).Communicator returns return object to stub, then to client.

[1][2]by checking an object:
(1.if it is remote, create a stub by creating a dispatcher on default port and exportto it, pass the stub by serialization.
(2 if it is serializable, pass by serialization . 
(3 if neigther, exception

## Code Components ##

**<1> Remote640**

--flagging empty interface , acts like Remote in java library

**<2> Remote640Stub**

--class extended by all XX_stubs

--contains helper methods for marshalling, checking and sending.

**<3> RemoteObjectReference(ROR)**

--uniquely identifies an remote object

--contains ip,port,uuid(object_key),interfaceName

**<4>Message**

--marshalled method call by stub, passed through network by serialization.

--contains ROR,methodname,checked parameter objects, whether has return.

**<5>Communicator**

--for client,pass message to server, by looking at message.ror.host..

--for server,set up listening server for every registered dispatcher,once it accepts a socket, spawn a thread of handling server to read in message, pass it to dispatcher, get response and write out message.

--notes that when client passes an callback object(remote object as argument for a remote method), client sets up a "implicit proxy dispacher"

--acts like Communicator in ICE.

**<6> ProxyDispacther(Dispacther)**

--takes a port, administrates multiple remote objects.

--holds a Map for <uuid, localRef> of remote objects

--when get a message,unmarshall it, get the localRef by checking the Map for message.ROR.uuid

--invokes method,checks return value, pass back to communicator.

--acts like ObjectAdapter in ICE.

**<7> RegistryClient**

--on client side: send lookup request to registry

--on server side: send bind request to registry

**<8> LocateRegistry**

--factory class

--builds RegistryClient and RegistryServer

--check registry address

**<9>RegistryServer**

--listens on a well-kown port

--responds to “LocateRegistry” query

--responds to “lookup” and “rebind” query

--owns a table that maps string name to stub.ROR


**<10>XX_stub**

--possibly could be compliled, not done.

--implements the remote obj’s remote interface,extends Remote640

--contains every method that could be remotely called

--once a method is called, set parameter, returnVal, methodName, then call send(marshall()).





## Code Architecture ##

### rmi ###
contains code for rmi common-environment
### rmi-testclient ###
contains code for testing client using rmi, includes remoteObj,remoteObjStub,ClientDriver and CallBackObj implementation
### rmi-testserver###
contains code for testing server using rmi, server hosts serveral remoteObjects, includes remoteObj implementation, CallBackObjStub(client) and ServerDriver.

## How to run ##
**TO GENERATE STUB**

cd rmi-stubcompiler

mvn clean install

mvn exec:java -Dexec.args="<path to your interface>"


**TO COMPILE**

cd rmi

mvn clean install

cd ../rmi-testclient

mvn clean install

cd ../rmi-testserver

mvn clean install

**TO RUN CLIENT**

cd rmi-testclient

mvn exec:java -Dexec.args="<registryIpAddr> <registryPort>"


**TO RUN SERVER**

cd rmi-testserver

mvn exec:java -Dexec.args="<registryIpAddr>"

**TO TEST(after lanching client and server)**

LDISP <string>: display the input string on serverSide

LECHO <string>: server echo input string back to client

LCALLBACK : pass a remote call back obj to server as argument, let server callback on it


# To be done #
<1> currently used static synchronized lock on Communicator.sendMessage() to avoid two sent messages returns in a different order(especially when the first one needs to wait for returning a big return object), could be updated by using a non-static lock on
send method in Remote640Stub. But that requires an remote object instance always creates the same stub instance. Possibly needs a Map<objLocalRef,objStubRef>

<2> the registry server needs thread pooling, or non-blocking IO to handle intense request

<3> implement shutdown hook for servers.


# Q&A #

??what would happen if an remote object is returned but not exported?

When passing an exported remote object as a parameter or return value in a remote method call, the stub for that remote object is passed instead. Remote objects that are not exported will not be replaced with a stub instance. A remote object passed as a parameter can only implement remote interfaces.

??why use different port and host addr for ROR?

multiple remotes objects can have the same hosts and port name. a port and host addr could identify a dispatcher, which could hold multiple remote objects.

??Are remote objects all Singletons when using RMI?

you can only bind one object in the registry under a given name. But the object you bind could itself be a proxy to your own object pool, for example using the Spring AOP CommonsPoolTargetSource mechanism.


# reference #
[1]JAVA RMI  http://docs.oracle.com/javase/tutorial/rmi/

[2] ZEROC ICE http://doc.zeroc.com/display/Doc/Home