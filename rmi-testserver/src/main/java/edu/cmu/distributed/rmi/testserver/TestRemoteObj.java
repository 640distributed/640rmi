package edu.cmu.distributed.rmi.testserver;

import edu.cmu.distributed.rmi.Remote640;
import edu.cmu.distributed.rmi.testclient.TestRemoteCallbackObj;


public interface TestRemoteObj extends Remote640 {
	public static final String NAME="engine";

	public void display(String str);
	
	public String echo(String str);
	
	public void callBack(TestRemoteCallbackObj callbk);
}
