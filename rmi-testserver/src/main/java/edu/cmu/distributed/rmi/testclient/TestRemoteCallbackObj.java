package edu.cmu.distributed.rmi.testclient;

import edu.cmu.distributed.rmi.Remote640;


public interface TestRemoteCallbackObj extends Remote640 {
	
	public static final String NAME="callback";
	public void displayState();
	
	public void updateState(Integer state);
	
}
