package edu.cmu.distributed.rmi.driver;

import java.io.IOException;

import edu.cmu.distributed.rmi.Communicator;
import edu.cmu.distributed.rmi.LocateRegistry;
import edu.cmu.distributed.rmi.ProxyDispatcher;
import edu.cmu.distributed.rmi.RegistryClient;
import edu.cmu.distributed.rmi.testserver.TestRemoteObj;
import edu.cmu.distributed.rmi.testserver.TestRemoteObj_imp;

public class ServerDriver {

	public static void main(String[] args){
		
		if(args.length!=2){
			System.out.println("Args:<registryPort> <serverPort>");
			System.exit(1);
		}
		
		int regPort=Integer.parseInt(args[0]);
		int serverPort=Integer.parseInt(args[1]);

		LocateRegistry.buildRegistryServer(regPort);
		System.out.println("registry server build successfully...");
		RegistryClient registry=null;
		TestRemoteObj engine=new TestRemoteObj_imp();
		try {
			registry=LocateRegistry.getRegistry("localhost", regPort);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		//System.out.println(registry==null);
		try {
			ProxyDispatcher disp=Communicator.createDispatcher(serverPort);
			registry.rebind(TestRemoteObj.NAME, disp.export(1,engine));
			System.out.println("server set up successfully,waiting for request...");
			while(true){
				Thread.sleep(1000);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}
}
