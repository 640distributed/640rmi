package edu.cmu.distributed.rmi.testserver;

import java.util.ArrayList;
import java.util.Arrays;

import edu.cmu.distributed.rmi.Remote640Stub;
import edu.cmu.distributed.rmi.RemoteObjectRef;
import edu.cmu.distributed.rmi.testclient.TestRemoteCallbackObj;

public class TestRemoteObj_imp_stub extends Remote640Stub implements TestRemoteObj {
	
	/**
	 * 
	 //*/
	private static final long serialVersionUID = 8518716045054443910L;

	public TestRemoteObj_imp_stub(RemoteObjectRef ror){
		super(ror);
	}

	public void display(String str) {
		methodName="display";
		parameters=new ArrayList<Object>(Arrays.asList(str));	
		hasReturn=false;

		try {
			System.out.println("sent display!");
			send(marshall());
		} catch (Exception e) {
			e.getMessage();
			e.printStackTrace();
		}
		
	}

	public String echo(String str) {
		methodName="echo";
		parameters=new ArrayList<Object>(Arrays.asList(str));
		hasReturn=true;
		
		String returnObj=null;
		try {
			returnObj=(String)send(marshall());
		} catch (Exception e) {
			e.getMessage();
			e.printStackTrace();
		}
		return returnObj;
	}

	public void callBack(TestRemoteCallbackObj callbk) {
		methodName="callBack";
		parameters=new ArrayList<Object>(Arrays.asList(callbk));
		hasReturn=false;
		try {
			send(marshall());
		} catch (Exception e) {
			e.getMessage();
			e.printStackTrace();
		}
		
	}
	

}
