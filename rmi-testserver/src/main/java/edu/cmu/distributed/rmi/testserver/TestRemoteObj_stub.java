package edu.cmu.distributed.rmi.testserver;

import java.util.*;
import edu.cmu.distributed.rmi.Remote640Stub;
import edu.cmu.distributed.rmi.RemoteObjectRef;
import edu.cmu.distributed.rmi.Remote640;
import edu.cmu.distributed.rmi.testclient.TestRemoteCallbackObj;


/**
 *
 *
 *    this is an auto-generated stub by stub-compiler 
 *
 *    @author: qingzhou  
 *
 *
 */

public class TestRemoteObj_stub extends Remote640Stub implements TestRemoteObj
{



	public TestRemoteObj_stub(RemoteObjectRef ror)
	{
		super(ror);
	}


	public void display (String str)
	{

		methodName="display";
		parameters=new ArrayList<Object>(Arrays.asList(str));
		hasReturn=false;

		/***********marshall&send***********/
		try{
			send(marshall());
		} catch(Exception e){
			e.printStackTrace();
		}

	}


	public String echo (String str)
	{

		methodName="echo";
		parameters=new ArrayList<Object>(Arrays.asList(str));
		hasReturn=true;
		String returnObj=null;

		/***********marshall&send***********/
		try{
			returnObj=(String)send(marshall());
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}

		return returnObj;
	}


	public void callBack (TestRemoteCallbackObj callbk)
	{

		methodName="callBack";
		parameters=new ArrayList<Object>(Arrays.asList(callbk));
		hasReturn=false;

		/***********marshall&send***********/
		try{
			send(marshall());
		} catch(Exception e){
			e.printStackTrace();
		}

	}




}
