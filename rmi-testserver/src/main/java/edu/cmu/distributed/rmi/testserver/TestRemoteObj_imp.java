package edu.cmu.distributed.rmi.testserver;

import java.io.IOException;

import edu.cmu.distributed.rmi.*;
import edu.cmu.distributed.rmi.testclient.TestRemoteCallbackObj;


public class TestRemoteObj_imp implements TestRemoteObj {

	public void display(String str) {
		System.out.println("\n\n**** Display info from client ****\n\n");
		System.out.println(str+"\n\n");
		
	}

	public String echo(String str) {
		System.out.println("\n\n**** Echoed from Server ****\n\n"+str+"\n\n");
		return new String(str);
	}

	public void callBack(TestRemoteCallbackObj callbk) {
		System.out.println("\n\n**** Server callback remote obj from client ****\n\n");
		callbk.displayState();
		callbk.updateState(3737);
		callbk.displayState();	
	}
	

}
