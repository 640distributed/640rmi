package edu.cmu.distributed.rmi.testserver;

import edu.cmu.distributed.rmi.Remote640;
import edu.cmu.distributed.rmi.testclient.TestRemoteCallbackObj;

/**
 * this is a test remote object interface
 * notes it extends Remote640
 * 
 * It should appear on both client and server side
 * client uses it to compile a stub
 * 
 * @author qing
 *
 */
public interface TestRemoteObj extends Remote640 {

	public static final String NAME="engine";
	public void display(String str);
	
	public String echo(String str);
	
	public void callBack(TestRemoteCallbackObj callbk);
}
