package edu.cmu.distributed.rmi.testclient;

public class TestRemoteCallbackObj_imp implements TestRemoteCallbackObj{

	private int state=0;
	public void displayState() {
		System.out.println("\n\n**** Display State from Server Callback****\n\n");
		System.out.println("State: "+state+"\n\n");
		
	}

	public void updateState(Integer state) {
		this.state=state;
		System.out.println("\n\n**** State changed! ****\n\n");
		
	}

}
