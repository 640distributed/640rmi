package edu.cmu.distributed.rmi.testclient;

import java.util.*;
import edu.cmu.distributed.rmi.Remote640Stub;
import edu.cmu.distributed.rmi.RemoteObjectRef;
import edu.cmu.distributed.rmi.Remote640;


/**
 *
 *
 *    this is an auto-generated stub by stub-compiler 
 *
 *    @author: qingzhou  
 *
 *
 */

public class TestRemoteCallbackObj_stub extends Remote640Stub implements TestRemoteCallbackObj
{



	public TestRemoteCallbackObj_stub(RemoteObjectRef ror)
	{
		super(ror);
	}


	public void displayState ()
	{

		methodName="displayState";
		parameters=new ArrayList<Object>(Arrays.asList());
		hasReturn=false;

		/***********marshall&send***********/
		try{
			send(marshall());
		} catch(Exception e){
			e.printStackTrace();
		}

	}


	public void updateState (Integer state)
	{

		methodName="updateState";
		parameters=new ArrayList<Object>(Arrays.asList(state));
		hasReturn=false;

		/***********marshall&send***********/
		try{
			send(marshall());
		} catch(Exception e){
			e.printStackTrace();
		}

	}




}
