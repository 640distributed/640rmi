package edu.cmu.distributed.rmi.testclient;

import edu.cmu.distributed.rmi.Remote640;

/**
 * this is a test callback object,
 * notes it extends Remoted640, 
 * on the client side,
 * and passed to remoteObject's testCallBack method 
 * as an argument
 * 
 * @author qing
 *
 */
public interface TestRemoteCallbackObj extends Remote640 {
	
	public void displayState();
	
	public void updateState(Integer state);
	
}
