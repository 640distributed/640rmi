package edu.cmu.distributed.rmi.driver;

import java.io.Console;
import java.io.IOException;

import edu.cmu.distributed.rmi.*;
import edu.cmu.distributed.rmi.testclient.TestRemoteCallbackObj;
import edu.cmu.distributed.rmi.testclient.TestRemoteCallbackObj_imp;
import edu.cmu.distributed.rmi.testserver.TestRemoteObj;



public class ClientDriver {
	
	public static void main(String[] args){
		
		if(args.length!=2){
			System.out.println("Args: <registryIpAddr> <registryPort>");
			System.exit(1);
		}
			
		boolean help=true;
		Console console = System.console();
		TestRemoteObj engine=null;
		try {
			RegistryClient registry=LocateRegistry.getRegistry(args[0],Integer.parseInt(args[1]));
			engine = (TestRemoteObj) registry.lookup(TestRemoteObj.NAME);
		} catch (Exception e) {
			
			e.printStackTrace();
			System.exit(1);
		}
		System.out.println("stub set up properlys...");
		
		while(true){
			
			if(help){
				System.out.println("============ Choose from following options ===============");
				System.out.println("LDISP <string>: display the input string on serverSide");
				System.out.println("LECHO <string>: server echo input string back to client");
				System.out.println("LCALLBACK : pass a remote obj to server as argument, let server callback on it");
				System.out.println("HELP : help");
				System.out.println("\n\n");
				help=false;
			}
			System.out.println("Enter command:");
			String input = console.readLine();
			String cmd=input.split(" ")[0];
			
			if(cmd.equals("LDISP")&&input.split(" ").length>=2){
				String arg=input.substring(input.indexOf(" ")+1);
					
				
				engine.display(arg);
			}
			else if(cmd.equals("LECHO")&&input.split(" ").length==2){
						
				String arg=input.substring(input.indexOf(" ")+1);	
				String result=engine.echo(arg);
				System.out.println("\n\n**** got Echoed String from Server ****\n\n"+result+"\n\n");
			}
			else if(cmd.equals("LCALLBACK")){
				TestRemoteCallbackObj callbk=new TestRemoteCallbackObj_imp();
				System.out.println("\n\n**** sent call back remote obj to Server ****\n\n");
				engine.callBack(callbk);
			}
			else if(cmd.equals("HELP")){
				help=true;
			}
		}
		
		/*RegistryClient registry=LocateRegistry.getRegistry("localhost",8899);
		TestRemoteObj engine=null;
		try {
			engine = (TestRemoteObj) registry.lookup(TestRemoteObj.NAME);
		} catch (Exception e) {
			
			e.printStackTrace();
			System.exit(1);
		}
		String arg="hello";
		engine.display(arg);
		*/
	}
	
	
}
