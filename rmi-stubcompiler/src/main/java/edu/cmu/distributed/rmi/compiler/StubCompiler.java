package edu.cmu.distributed.rmi.compiler;

import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;
import japa.parser.ast.ImportDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.Parameter;
import japa.parser.ast.visitor.VoidVisitorAdapter;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;




public class StubCompiler {
	
	private  CompilationUnit cu;
	private PrintWriter out;
	private StringBuilder strBuilder;
	
	private String packageName;
	private List<String> imports;
	private String interfaceName;
	private String stubName;

	private List<Method> methods;
	private String classNotes="/**\n *\n *\n *    this is an auto-generated stub by stub-compiler \n *\n *    @author: qingzhou  \n *\n *\n */\n\n";

	
	public StubCompiler(String interfacePath) throws FileNotFoundException{
		
		String stubPath=interfacePath.replaceAll("\\.java","")+"_stub.java";
		//System.out.println("PATH: "+stubPath);
		this.imports=new ArrayList<String>();
		this.methods=new ArrayList<Method>();
		this.strBuilder=new StringBuilder();
			
		try {
			FileInputStream in = new FileInputStream(interfacePath);
			this.out=new PrintWriter(new FileWriter(stubPath,false));
			this.cu = JavaParser.parse(in);
		} catch (FileNotFoundException e) {	
			e.printStackTrace();
			throw new FileNotFoundException();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	}
	
	public void parseInterface(){
		
		this.packageName=cu.getPackage().toString();
		
		this.imports.add("import java.util.*;\n");
		this.imports.add("import edu.cmu.distributed.rmi.Remote640Stub;\n");
		this.imports.add("import edu.cmu.distributed.rmi.RemoteObjectRef;\n");
		for(ImportDeclaration ide:this.cu.getImports()){
			this.imports.add(ide.toString());
		}
		
		this.interfaceName=cu.getTypes().get(0).getName();
		this.stubName=this.interfaceName+"_stub";

		new MethodVisitor().visit(cu, null);
		
	}
	
	public void buildHeader(){
		this.strBuilder.append(this.packageName);
		
		for(String importstr:this.imports){
			this.strBuilder.append(importstr);
		}
		
		this.strBuilder.append("\n\n");
		this.strBuilder.append(this.classNotes);
		
		this.strBuilder.append("public class "+this.stubName+" extends Remote640Stub implements "+this.interfaceName+"\n");
		
		this.strBuilder.append("{\n\n\n\n");
	}
	
	public void buildConstructor(){
		this.strBuilder.append("\tpublic "+this.stubName+"(RemoteObjectRef ror)\n\t{\n\t\tsuper(ror);\n\t}\n\n\n");
	}
	
	public void buildMethod(Method mtd){
		this.strBuilder.append("\tpublic "+mtd.returnType+" "+mtd.methodName+" (");
		for(Argument arg:mtd.args){
			this.strBuilder.append(arg.argType+" "+arg.argName+",");
		}
		if(!mtd.args.isEmpty()){
			strBuilder.deleteCharAt(strBuilder.lastIndexOf(","));
		}
		this.strBuilder.append(")\n\t{\n\n");
		
		this.strBuilder.append("\t\tmethodName=\""+mtd.methodName+"\";\n");
		
		this.strBuilder.append("\t\tparameters=new ArrayList<Object>(Arrays.asList(");
		for(Argument arg:mtd.args){
			this.strBuilder.append(arg.argName+",");
		}
		if(!mtd.args.isEmpty()){
			strBuilder.deleteCharAt(strBuilder.lastIndexOf(","));
		}
		this.strBuilder.append("));\n");
		
		String hasReturn=mtd.returnType.equals("void")?"false":"true";
		this.strBuilder.append("\t\thasReturn="+hasReturn+";\n");
		
		if(!mtd.returnType.equals("void")){
			this.strBuilder.append("\t\t"+mtd.returnType+" returnObj=null;\n");
		}
		
		this.strBuilder.append("\n\t\t/***********marshall&send***********/\n");
		this.strBuilder.append("\t\ttry{\n\t\t\t");
		if(!mtd.returnType.equals("void")){
			this.strBuilder.append("returnObj=("+mtd.returnType+")");
		}
		this.strBuilder.append("send(marshall());\n\t\t} catch(Exception e){\n\t\t\te.printStackTrace();\n");
		if(!mtd.returnType.equals("void")){
			this.strBuilder.append("\t\t\treturn null;\n");
		}
		this.strBuilder.append("\t\t}\n\n");
		
		if(!mtd.returnType.equals("void")){
			this.strBuilder.append("\t\treturn returnObj;\n");
		}
		
		this.strBuilder.append("\t}\n\n\n");
	}
	
	public void buildTailer(){
		this.strBuilder.append("\n\n}\n");
	}
	
	public void outputFile(){
		out.print(this.strBuilder.toString());
		out.flush();
	}
	
	public static void runCompiler(String inpath) throws FileNotFoundException{
		StubCompiler stb=new StubCompiler(inpath);
		stb.parseInterface();
		stb.buildHeader();
		stb.buildConstructor();
		for(Method mtd:stb.methods){
			stb.buildMethod(mtd);
		}
		stb.buildTailer();
		stb.outputFile();
	}
	
	public static void main(String[] args){
		//String inpath="/Users/qing/projects/distributed/640rmi/rmi-testclient/src/main/java/edu/cmu/distributed/rmi/testclient/TestRemoteCallbackObj.java";
		//String outpath="src/main/java/edu/cmu/distributed/rmi/compiler/TestRemoteObjnewStub.java";
		if (args.length!=1){
			System.out.println("invalid arg!");
		}
		
		try {
			runCompiler(args[0]);
			//runCompiler(inpath);
		} catch (FileNotFoundException e) {
			System.out.println("INPUT FILE PATH INVALID!");
			e.printStackTrace();
		}
		
	}
	
   class MethodVisitor extends VoidVisitorAdapter {
        @Override
        public void visit(MethodDeclaration n, Object obj) {
        	Method method=new Method();
        	method.methodName=n.getName();
        	method.args=new ArrayList<Argument>();
        	if(n.getParameters()!=null){
        		for(Parameter p:n.getParameters()){
        			Argument arg=new Argument();
        			arg.argType=p.getType().toString();
        			arg.argName=p.toString().split(" ")[1];
        			method.args.add(arg);
        		}
        	}
        	method.returnType=n.getType().toString();
        	methods.add(method);
 
        }
    }
    
	static class Method{
		String methodName;
		String returnType;
		List<Argument> args;
	}
	
	static class Argument{
		String argType;
		String argName;
	}
	
}
