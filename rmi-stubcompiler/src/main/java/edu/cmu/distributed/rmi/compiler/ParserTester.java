package edu.cmu.distributed.rmi.compiler;

import japa.parser.JavaParser;
import japa.parser.ast.CompilationUnit;
import japa.parser.ast.Node;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.Parameter;
import japa.parser.ast.body.TypeDeclaration;
import japa.parser.ast.visitor.VoidVisitorAdapter;

import java.io.FileInputStream;

public class ParserTester {
	 public static void main(String[] args) throws Exception {
	        // creates an input stream for the file to be parsed
	        FileInputStream in = new FileInputStream("src/main/java/edu/cmu/distributed/rmi/compiler/TestRemoteObj.java");

	        CompilationUnit cu;
	        try {
	            // parse the file
	            cu = JavaParser.parse(in);
	        } finally {
	            in.close();
	        }

	        // prints the resulting compilation unit to default system output
	        System.out.println(cu.getImports());
	        System.out.println("MYNAME IS "+cu.getTypes().get(0).getName());
	        System.out.println(cu.getPackage());

	        new MethodVisitor().visit(cu, null);
}
	 
	 /**
	     * Simple visitor implementation for visiting MethodDeclaration nodes. 
	     */
	    private static class MethodVisitor extends VoidVisitorAdapter {

	        @Override
	        public void visit(MethodDeclaration n, Object arg) {
	            // here you can access the attributes of the method.
	            // this method will be called for all methods in this 
	            // CompilationUnit, including inner class methods
	            System.out.println(n.getName());
	            //System.out.println(n.getParameters());
	            for(Parameter p:n.getParameters()){
	        		System.out.println(p.toString().split(" ")[1]);
	        	}
	            System.out.println("RETTYPE is " + n.getType());
	            
	            System.out.println("visit ends!");
	        }
	    }
}
