package edu.cmu.distributed.rmi;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The communicator class implements the socket IO communication for RMI,
 * for each ProxyDispacher, it opens its specified port and listens for incoming Messages,
 * once accepted, pass that Message Object to corresponding ProxyDispather.
 * 
 * 
 * @author qing
 *
 */
public class Communicator {
	
	public static int defaultPort=10000;
	
	public static Map<Integer,ListeningServer> listeningServers
						=new HashMap<Integer,ListeningServer>();
	
	public static Map<Integer,ProxyDispatcher> dispatcherPortMap
						=new HashMap<Integer,ProxyDispatcher>();
	
	public static ProxyDispatcher createDispatcher(){
		int port=defaultPort;
		ServerSocket ss=null;
		boolean retry=true;
		while(retry){
			retry=false;
			while(dispatcherPortMap.keySet().contains(port)){
				port++;
			}
			//probe binding
			try {
				ss=new ServerSocket(port);
			} catch (IOException e) {
				port++;
				retry=true;
			}
		}
		
		try {
			ss.close();
		} catch (IOException e){
			e.printStackTrace();
			throw new RuntimeException("error closing probing sock");
		}
		
		Communicator.defaultPort=port;
		
		return  createDispatcher(port);
	}
	
	public static ProxyDispatcher createDispatcher(int port){
		if(!dispatcherPortMap.keySet().contains(port)){
			ProxyDispatcher disp=new ProxyDispatcher(port);
			dispatcherPortMap.put(port, disp);
			serverSetup(port);
			return disp;
		}
		return null;
	}
		
	
	public static ProxyDispatcher getDispatcher(int port){
		if(dispatcherPortMap.keySet().contains(port)){
			return dispatcherPortMap.get(port);
		}
		else
			return null;
	}
	
	public static void closeDispatcher(int port){
		dispatcherPortMap.keySet().remove(port);
		ListeningServer lserver=listeningServers.get(port);
		listeningServers.keySet().remove(port);
		try {
			lserver.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	//TODO change static locking!
	public synchronized static Object sendMessage(Message msg) throws Exception{
		String addr=msg.ror.host;
		int port=msg.ror.port;
		Object returnValue=null;
		OutputStream out=null;
		InputStream in=null;
		try {
			Socket socket=new Socket(addr,port);
			socket.setSoTimeout(5000);
			out=socket.getOutputStream();
			in=socket.getInputStream();
			ObjectOutputStream objOut=new ObjectOutputStream(out);
			objOut.writeObject(msg);

			ObjectInputStream objIn=new ObjectInputStream(in);
			Object result=objIn.readObject();
			if(msg.hasReturn){			
				returnValue=result;
			}
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			out.close();
			throw new Exception("Server not responding when calling method!");
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return returnValue;
	}
	
	
	private static void serverSetup(int port){
		listeningServers.put(port,new ListeningServer(port));
		
	}
	
		
}


class ListeningServer extends Thread{
	private int port;
	private ServerSocket s;
	private boolean close=false;
	public ListeningServer(int port){
		this.port=port;
		this.start();
	}
	
	public void close() throws IOException{
		close=true;
		s.close();	
	}
	
	//TODO addShutDownHook
	@Override
	public void run(){
	
		try {
			 s=new ServerSocket(port);
			 while(!close){
				 Socket socket=s.accept();
				 if(!s.isClosed()){
					 new HandlingServer(socket,port);
				 }
			 }
			 
		}catch(IOException e){
			//e.printStackTrace();
		}finally{
			if(s!=null && !s.isClosed()){
				try {
					s.close();
				} catch (IOException e){
					e.printStackTrace();
				}
			}
		}
	}
}

class HandlingServer extends Thread{
	
	private Socket socket;
	private int listenPort;
	public HandlingServer(Socket socket,int listenport){
		this.socket=socket;
		this.listenPort=listenport;
		this.start();
	}
	
	@Override
	public void run(){
		
		try {
			ObjectInputStream in=new ObjectInputStream(socket.getInputStream());	
			Message msg=(Message)in.readObject();
			//System.out.println("has return?"+msg.hasReturn+":name "+msg.methodName);
			ProxyDispatcher disp=Communicator.dispatcherPortMap.get(listenPort);
			Object returnVal=disp.unmarshalAndInvoke(msg);
			
			ObjectOutputStream out=new ObjectOutputStream(socket.getOutputStream());
			out.writeObject(ProxyDispatcher.marshallReturnObj(returnVal));
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch(ClassNotFoundException e){
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
	}
}

