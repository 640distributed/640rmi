package edu.cmu.distributed.rmi;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This abstract class provides the common methods for all "Stubs". Stub 
 * for any remote class must extend this class
 * 
 * @author qing
 *
 */
public abstract class Remote640Stub implements Remote640,Serializable{
	

	private static final long serialVersionUID = 8917313565811334023L;
	
	protected String methodName;
	protected List<?> parameters;
	//TODO handle argument null;use class loader
	protected List<String> parameterClasses;
	protected boolean hasReturn;
	protected RemoteObjectRef ror;
	
	protected Remote640Stub(RemoteObjectRef ror){
		this.ror=ror;
	}
	
	protected Message marshall() throws Exception{
		if(parameters!=null){
			List<Object> temp=new ArrayList<Object>();
			for(Object para:parameters)
				temp.add(checkObj(para));
			parameters=temp;
		}
		
		return new Message(methodName,parameters,hasReturn,ror);
	}
	
	protected Object send(Message msg) throws Exception{
		Object retobj=Communicator.sendMessage(msg);
		if(retobj instanceof NoSuchFieldException){
			return null;
		}
		else if(retobj instanceof Exception){
			throw (Exception) retobj;
		};
		return retobj;
	}

	private static Remote640Stub prepareRemoteObjStub(Remote640 remoteObj){
		ProxyDispatcher disp=null;
		RemoteObjectRef ror=null;
		String className;
		Remote640Stub stub=null;
		try {
			disp=Communicator.getDispatcher(Communicator.defaultPort);
			if(disp==null){
				//System.out.println("call createDISP");
				disp=Communicator.createDispatcher();
			}
			ror=disp.export(UUID.randomUUID().getMostSignificantBits(),(Remote640)remoteObj);
			className=remoteObj.getClass().getName();
			stub=(Remote640Stub) Class.forName(className.replaceAll("_imp", "")+"_stub")
					.getDeclaredConstructor(RemoteObjectRef.class)
					.newInstance(ror);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}	
		return stub;
		
	}
	//used both by stub and ProxyDispatcher
	static Object checkObj(Object obj) throws Exception{
		Object retObj=null;
		if(obj == null){
			obj=new NoSuchFieldException();
		}
		else if(obj instanceof Remote640 ){
			retObj=prepareRemoteObjStub((Remote640)obj);
		}
		else if(obj instanceof Serializable){
			retObj=obj;
		}
		else{
			throw new Exception("Illegle Object type, neither serializable nor remote!["+obj.getClass().getName()+"]");
		}
		
		return retObj;
	}
	/*
	public static final Remote640Stub buildStub(RemoteObjectRef ror) throws ClassNotFoundException{
		Class<?> c;
		Constructor<?> ctor;
		Remote640Stub stub=null;
		try {
			c=Class.forName(ror.interfaceName+"_stub");
			ctor=c.getDeclaredConstructor(RemoteObjectRef.class);
			stub= (Remote640Stub) ctor.newInstance(ror);
		} catch (ClassNotFoundException e) {
			throw new ClassNotFoundException();
		} catch (Exception e){
			e.printStackTrace();
			throw new RuntimeException();
		}
		return stub;
	}
	 */
}
