package edu.cmu.distributed.rmi;

import java.io.Serializable;
import java.util.List;

/**
 * Message contains the marshalled RPC arguments to
 * be invoked on a method of a remote object
 * 
 * @author qing
 *
 */
public class Message implements Serializable{
	
	private static final long serialVersionUID = -3271308846194679203L;
	String methodName;
	 List<?> parameters;
	 boolean hasReturn;
	 RemoteObjectRef ror;
	
	public Message(String methodName,List<?> parameters,
			boolean hasReturn,RemoteObjectRef ror){
		this.parameters=parameters;
		this.methodName=methodName;
		this.hasReturn=hasReturn;
		this.ror=ror;
	}
}
