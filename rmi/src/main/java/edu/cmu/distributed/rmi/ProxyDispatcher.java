package edu.cmu.distributed.rmi;

import java.lang.reflect.*;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
/**
 * The ProxyDispacher works as a "Endpoint container" for remote objects.
 * Multiple remote objects can bind(export) to a proxyDispacher using its specified IP and Port
 * (probably protocal in future).
 * 
 * Also the ProxyDispacher unmarshalls and invokes the remote object using
 * the message it got from Communicator.
 * 
 * @author qing
 *
 */
public class ProxyDispatcher {
	
	private int port;
	
	private  Map<Long,Remote640> dispatcherMap;
	
	ProxyDispatcher(int port){
		this.port=port;
		dispatcherMap=new HashMap<Long,Remote640>();
	}
	
	public int getPort(){
		return this.port;
	}
	
	public RemoteObjectRef export(Remote640 localref) throws Exception{
		return export(UUID.randomUUID().getLeastSignificantBits(),localref);
	}
	
	public RemoteObjectRef export(long objectKey,Remote640 localref) throws Exception{
		
		dispatcherMap.put(objectKey, localref);
		
		String className=localref.getClass().getName();
		
		return new RemoteObjectRef
				(InetAddress.getLocalHost().getHostAddress(),port,className,objectKey);
		
	}
	
	public void unexport(Remote640 localref) throws Exception{
		boolean contains=false;
		Remote640 storedRef=null;
		List<Long>removedKey=new ArrayList<Long>();
		for(long objkey:dispatcherMap.keySet()){
			storedRef=dispatcherMap.get(objkey);
			if(storedRef==localref){
				contains=true;
				removedKey.add(objkey);
			}
		}
		if(!contains){
			throw new Exception("No exported obj of the dispacher matches input referencer!");
		}
		else{
			dispatcherMap.keySet().removeAll(removedKey);
			if(dispatcherMap.keySet().isEmpty()){
				Communicator.closeDispatcher(port);
			}
		}
	}
	
	//TODO the same localref should get the same stub, for locking purpose
	/*
	public boolean checkExported(Remote640 localref){
		if(dispatcherMap.values().contains(localref)){
			return true;
		}
		else{
			return false;
		}
	}
	*/
	
	static Object marshallReturnObj(Object obj) throws Exception{
		
		return Remote640Stub.checkObj(obj);
	}
	
	private Method getInvocableMethod(Object targetObject, Object[] parameters,
	        String methodName) {
	    for (Method method : targetObject.getClass().getMethods()) {
	        if (!method.getName().equals(methodName)) {
	            continue;
	        }
	        Class<?>[] parameterTypes = method.getParameterTypes();
	        boolean matches = true;
	        for (int i = 0; i < parameterTypes.length; i++) {
	            if (!parameterTypes[i].isAssignableFrom(parameters[i]
	                    .getClass())) {
	                matches = false;
	                break;
	            }
	        }
	        if (matches) {
	           return method;
	        }
	    }
	    return null;
	}
	
	synchronized Object  unmarshalAndInvoke(Message msg) {
		//System.out.println("dispatcher got " + msg.methodName + "invoke!");
		Object returnVal=new Boolean(false);;

		if (dispatcherMap.containsKey(msg.ror.objectKey)) {
			// System.out.println("pass exported!");
			Remote640 localref = dispatcherMap.get(msg.ror.objectKey);
			List<Class<?>> paraClasses = new ArrayList<Class<?>>();
			try {
				Method method;
				if (msg.parameters == null) {
					method = localref.getClass().getDeclaredMethod(
							msg.methodName);
					returnVal = method.invoke(localref);
				}

				else {
					for (Object para : msg.parameters) {
						Class<?> paraC;
						if(para instanceof Remote640){
							paraC=para.getClass().getInterfaces()[0];
						}
						else{
							paraC=para.getClass();
						}
						paraClasses.add(paraC);
					}
					try{
						method = localref.getClass().getDeclaredMethod(
							msg.methodName, paraClasses.toArray(new Class[0]));
					}catch (NoSuchMethodException nse){
						method=getInvocableMethod(localref,msg.parameters.toArray(),msg.methodName);
						if(method==null){
							throw new Exception("Can't find method!"+nse.getMessage());
						}
					}
					returnVal = method.invoke(localref,
							msg.parameters.toArray());
				}
				//if return Object is void, return true for hand-shaking, if has returnval, check 
				//whether it should be passed by reference or object
				if(!msg.hasReturn){
					returnVal=new Boolean(true);
				}
				else{
					returnVal=Remote640Stub.checkObj(returnVal);		
				}		

			}
			catch (Exception e) {
				returnVal=new Exception("error invocating on server!");
				e.printStackTrace();
			}
		}

		return returnVal;

	}

}
