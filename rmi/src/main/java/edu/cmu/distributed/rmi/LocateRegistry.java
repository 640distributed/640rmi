package edu.cmu.distributed.rmi;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * A factory class that builds registry client
 * and registry server.
 * 
 * @author qing
 *
 */
public class LocateRegistry {
	/**
	 * implement singleton pattern
	 */
	private static RegistryServer regServer;
	
	public static RegistryServer buildRegistryServer(int port){
		if(regServer==null){
			regServer=new RegistryServer("localhost",port);
		}
		return regServer;
	}
	
    public static RegistryClient getRegistry(String host, int port) throws Exception{
    	RegistryClient rc=null;
    	try{
    		Socket socket = new Socket(host, port);
    		socket.setSoTimeout(5000);

    		
	    	ObjectOutputStream objOut=new ObjectOutputStream(socket.getOutputStream());
	    	ObjectInputStream objIn=new ObjectInputStream(socket.getInputStream());
	    	
	    	RegistryInfoWrapper wrapper=
	    			new RegistryInfoWrapper("locate",null);

	    	objOut.writeObject(wrapper);
	    	wrapper=(RegistryInfoWrapper)objIn.readObject();
	    
	    
    		if (wrapper.result){
    			rc=new RegistryClient(host, port);
    		}
    		else{
    			rc=null;
    		}
    	}
    	catch (Exception e){ 
    		System.out.println(e.getMessage());
    		rc= null;
	    }
    	
    	if(rc==null){
    		throw new Exception("can not find registry by given name!");
    	}
    	else{
    		return rc;
    	}
    }
}
