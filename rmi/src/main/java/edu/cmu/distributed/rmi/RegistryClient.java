package edu.cmu.distributed.rmi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.AbstractMap;
import java.util.Map;

/**
 * The registryClient sends locate,lookup and 
 * bind requests to registryServer.
 * 
 * @author qing
 *
 */
public class RegistryClient {
	    private String host;
	    private int port;
	    
	    public static final String defaultHost="localhost";
	    public static final int defaultPort=8037;
	    		
	    
	    public RegistryClient(){
	    	this(defaultHost,defaultPort);
	    }
	    
	    public RegistryClient(String host, int port){
	    	this.host = host;
	    	this.port = port;
	    }
	    
	    public void closeRegistry() throws Exception{
	    	Socket socket = new Socket(host, port);
	    	ObjectOutputStream objOut=new ObjectOutputStream(socket.getOutputStream());
	    	
	    	RegistryInfoWrapper wrapper=
	    			new RegistryInfoWrapper("close",null);
	    	objOut.writeObject(wrapper);
	    }
	    
	    public Remote640 lookup(String remoteObjName) throws Exception{

	    	Socket socket = new Socket(host, port);

	    	ObjectOutputStream objOut=new ObjectOutputStream(socket.getOutputStream());
	    	ObjectInputStream objIn=new ObjectInputStream(socket.getInputStream());
	    	
	    	RegistryInfoWrapper wrapper=
	    			new RegistryInfoWrapper("lookup",new String(remoteObjName));
	    	objOut.writeObject(wrapper);
	    	try {
				wrapper=(RegistryInfoWrapper)objIn.readObject();
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			}
	    	
	    	Remote640 stub=null;

	    	if (wrapper.result){
	    		RemoteObjectRef ror;
	    		
				try {
					ror = (RemoteObjectRef) wrapper.obj;
					//TODO add search for stub in all classes in classpath
					Class<?> c=Class.forName(ror.interfaceName.replaceAll("_imp", "")+"_stub");
					Constructor<?> ctor=c.getDeclaredConstructor(RemoteObjectRef.class);
					stub=(Remote640) ctor.newInstance(ror);
				} catch (Exception e) {
					e.printStackTrace();
			    	socket.close();
					throw new RuntimeException();
					
				}
	    	}			
	    	else{
			//TODO implement exception
	    		throw new Exception("No remote object matches "+remoteObjName+" on registry!");
		    }
	    	socket.close();
	    	return stub;
	    }


	    
	    public void rebind(String roName, RemoteObjectRef ror) 
	    		throws IOException{
	    	
	    	Socket socket = new Socket(host, port);
	    	OutputStream out=socket.getOutputStream();
	    	ObjectOutputStream objOut=new ObjectOutputStream(out);
	    	RegistryInfoWrapper wrapper=
	    			new RegistryInfoWrapper("bind",new AbstractMap.SimpleEntry<String, RemoteObjectRef>(roName,ror));
	    	objOut.writeObject(wrapper);  	

	    } 
	    
	    public void unbind(String roName)
	    		throws IOException{
	    	Socket socket = new Socket(host, port);
	    	OutputStream out=socket.getOutputStream();
	    	ObjectOutputStream objOut=new ObjectOutputStream(out);
	    	
	    	RegistryInfoWrapper wrapper=
	    			new RegistryInfoWrapper("unbind",roName);
	    	objOut.writeObject(wrapper);  
	    }
	    
}

class RegistryInfoWrapper implements Serializable {
	String command;
	Object obj;
	boolean result;
	public RegistryInfoWrapper(String command,Object obj){
		this.command=command;
		this.obj=obj;
		this.result=false;
	}
	public void setResult(boolean result){
		this.result=result;
	}
	public void setObj(Object obj){
		this.obj=obj;
	}
	
}
