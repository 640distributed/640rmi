package edu.cmu.distributed.rmi;

import java.io.Serializable;


/**
 * RemoteObjectReference is the address of a remote object,
 * containing host,port,objectkey(for mapping on proxyDispacher)
 * and Name.
 * 
 * @author qing
 *
 */

public class RemoteObjectRef implements Serializable {

	private static final long serialVersionUID = 6162752734171717902L;
	 String host;
	 int port;
	 String interfaceName;
	 long objectKey;
	
	public RemoteObjectRef(String host,int port,String interfaceName,long objectKey){
		this.host=host;
		this.port=port;
		this.interfaceName=interfaceName;
		this.objectKey=objectKey;
		
	}
}
