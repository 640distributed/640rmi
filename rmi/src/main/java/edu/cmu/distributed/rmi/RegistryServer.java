package edu.cmu.distributed.rmi;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

/**
 * Starts a RMI registry server that listens for
 * locate,bind and lookup requests.
 * 
 * This could run on a different machine from 
 * client and server, could serve for multiple
 * client and server.
 * 
 * @author qing
 *
 */
public class RegistryServer extends Thread{
	

	private int port;
	private boolean exitserver=false;
	private ServerSocket s;
	
	private Map<String,RemoteObjectRef> registryTable;
		
	RegistryServer(String host,int port){
		this.registryTable=new HashMap<String,RemoteObjectRef>();
		this.port=port;
		this.start();	
	}
	
	public void close() throws IOException{
		exitserver=true;
		s.close();
	}
	
	
	@Override
	public void run(){
		try {
			s=new ServerSocket(port);
			
			//TODO implement thread pooling
			while(!exitserver){
				Socket socket=s.accept();
						
		    	ObjectOutputStream objOut=new ObjectOutputStream(socket.getOutputStream());
		    	ObjectInputStream objIn=new ObjectInputStream(socket.getInputStream());
				
		    	RegistryInfoWrapper wrapper=(RegistryInfoWrapper)objIn.readObject();
				
				String cmd=wrapper.command;
				//System.out.println(cmd);
				
				if(cmd.equals("locate")){
					wrapper.setResult(true);
					objOut.writeObject(wrapper);
				}
				
				else if(cmd.equals("lookup")){
					String objName=(String)wrapper.obj;
					if(registryTable.containsKey(objName)){
						
						wrapper.setResult(true);
						//TODO change Object to primitive
						RemoteObjectRef ror=registryTable.get(objName);
						
						//System.out.println(ror.interfaceName);
						wrapper.setObj(ror);
					}
					else{
						wrapper.setResult(false);
						wrapper.setObj(null);
					}
					objOut.writeObject(wrapper);
				}
				
				else if(cmd.equals("bind")){
					
					AbstractMap.SimpleEntry<String, RemoteObjectRef> entry=
							(AbstractMap.SimpleEntry<String, RemoteObjectRef>)wrapper.obj;
					String roName=entry.getKey();			
					RemoteObjectRef ror=entry.getValue();			
					registryTable.put(roName,ror);			
					//System.out.println("registered local ref: "+stub.ror.localReference);
					
				}
				else if(cmd.equals("unbind")){
					String roName=(String) wrapper.obj;
					if(registryTable.containsKey(roName)){
						registryTable.keySet().remove(roName);
					}
				}
				else if(cmd.equals("close")){
					this.close();
				}
						
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			//e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}finally{
			if(s!=null && !s.isClosed()){
				try {
					s.close();
				} catch (IOException e){
					e.printStackTrace();
				}
			}
		}
	} 
	
	public static void main(String[] args){
		new RegistryServer("localhost",8038);
	}
}
